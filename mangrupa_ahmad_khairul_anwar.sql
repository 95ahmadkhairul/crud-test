/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : mangrupa_ahmad_khairul_anwar

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-11-28 16:10:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for produk_ahmad
-- ----------------------------
DROP TABLE IF EXISTS `produk_ahmad`;
CREATE TABLE `produk_ahmad` (
  `id_produk` int(15) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(25) DEFAULT '',
  `harga_produk` int(25) DEFAULT NULL,
  `id_user` varchar(15) DEFAULT '',
  `create_time_produk` datetime DEFAULT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of produk_ahmad
-- ----------------------------
INSERT INTO `produk_ahmad` VALUES ('3', 'Kaos', '50000', '1', '2019-11-28 09:17:00');
INSERT INTO `produk_ahmad` VALUES ('6', 'Celana', '50000', '1', '2019-11-28 03:29:46');
INSERT INTO `produk_ahmad` VALUES ('8', 'Celana', '500000', '1', '2019-11-28 03:42:36');
INSERT INTO `produk_ahmad` VALUES ('10', 'Kaos', '50000', '1', '2019-11-28 03:42:55');

-- ----------------------------
-- Table structure for transaksi_ahmad
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_ahmad`;
CREATE TABLE `transaksi_ahmad` (
  `nama_customer` varchar(30) NOT NULL DEFAULT '',
  `no_telp_customer` varchar(15) NOT NULL DEFAULT '',
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_penjualan` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `harga_produk` int(30) NOT NULL,
  `discount_produk` int(30) NOT NULL,
  `nama_produk` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of transaksi_ahmad
-- ----------------------------
INSERT INTO `transaksi_ahmad` VALUES ('khairul', '081220434354', '1', '2019-11-28 14:34:59', '1', '1', '50000', '10', 'Baju');
INSERT INTO `transaksi_ahmad` VALUES ('khairul', '081220434354', '3', '2019-11-28 14:34:59', '1', '1', '50000', '10', 'Baju');
INSERT INTO `transaksi_ahmad` VALUES ('khairul', '081220434354', '4', '2019-11-28 14:34:59', '1', '1', '50000', '10', 'Baju');

-- ----------------------------
-- Table structure for user_ahmad
-- ----------------------------
DROP TABLE IF EXISTS `user_ahmad`;
CREATE TABLE `user_ahmad` (
  `id_user` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_ahmad
-- ----------------------------
INSERT INTO `user_ahmad` VALUES ('1', 'ahmad', 'haysIQ.UozcrI');
