<?php
include "connect.php";
$result = mysqli_query($mysqli, "select * from transaksi_ahmad");
session_start();
if ($_SESSION['user'] == ""){
	header('Location: login.php');
}else{
?>
<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
		<title>Ujian</title>
	</head>
	<style>
	table, th, tr, td{
		border:1px solid black;
		text-align: center;
	}
	th, td{
		padding:10px;
	}
	</style>
	<body>
		<a href="transaksi-create.php">Tambah data</a><br /><br />
		<table>
			<tr>
				<th>No</th>
				<th>Nama Customer</th>
				<th>No Telp Customer</th>
				<th>Tanggal Penjualan</th>
				<th>Nama Produk</th>
				<th>Harga Produk</th>
				<th>Diskon</th>
				<th colspan="2">Aksi</th>
			</tr>
				<?php $no = 1;
				while ($data = mysqli_fetch_array($result)) {
				echo"<tr>";
					echo "<td>".$no."</td>";										
					echo "<td>".$data['nama_customer']."</td>";
					echo "<td>".$data['no_telp_customer']."</td>";
					echo "<td>".$data['tanggal_penjualan']."</td>";
					echo "<td>".$data['nama_produk']."</td>";
					echo "<td>".$data['harga_produk']."</td>";	
					echo "<td>".$data['discount_produk']."% </td>";
					echo "<td><a href='transaksi-delete.php?id=".$data['id_transaksi']."'>Delete</a></td>";	
					echo "<td><a href='transaksi-edit.php?id=".$data['id_transaksi']."'>Edit</a></td>";			
				echo"</tr>";
				$no++;							
				}  ?>
		</table>
		<br />
	</body>
</html>
<?php
}
?>